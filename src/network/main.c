#include "listener/listener.h"
#include "sender/sender.h"
#include "GLOBALS.h"
#include "payload_gen/payload_gen.h"
#include <pthread.h>
#include <unistd.h>

void* call_send(void* arguments)
{
    struct dest_info* args=arguments;
    for(;;)
    {
	sleep(1);
	send_msg(args->string, args->ip, args->port);
    }
}

void* call_listen(void* arguments)
{
    struct local_info* args=arguments;
    start_listener(args->port, args->buffer);
}

void main()
{
    char buffer[PAYLOAD_SIZE];

    struct dest_info dst_inf;
    dst_inf.ip="127.0.0.1";
    dst_inf.port=1324;
    dst_inf.string=malloc(PAYLOAD_SIZE*sizeof(char));
    payload_gen(dst_inf.string, "Anon", "03:24", "OK");

    struct local_info lcl_inf;
    lcl_inf.port=1324;
    lcl_inf.buffer=buffer;

    pthread_t th1,th2;
    pthread_create(&th2,NULL,call_listen, (void*)&lcl_inf);
    pthread_create(&th1,NULL,call_send,(void*)&dst_inf);

    pthread_join(th2,NULL);
    pthread_join(th1,NULL);
}
