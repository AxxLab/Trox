#include "listener.h"

int start_listener(int port, char* buffer)
{
    struct sockaddr_in localAddr, sourceAddr;
    int sockfd, srcLen;

    if((sockfd = socket(AF_INET, SOCK_DGRAM, 0)) == -1)
    {
	    perror("Socket failed:");
	    return 0;
    }

    memset((char*)&localAddr,0,sizeof(localAddr));

    localAddr.sin_family = AF_INET;
    localAddr.sin_port = htons(port);
    localAddr.sin_addr.s_addr = htonl(INADDR_ANY); //TODO: Listener shouldn't listen to every source as it can be abused

    if((bind(sockfd,(struct sockaddr *) &localAddr,sizeof(localAddr))) == -1)
    {
	perror("Binding failed: ");
	return 0;
    }

    srcLen = sizeof(sourceAddr);

    for(;;)
    {
	if((recvfrom(sockfd, buffer, PAYLOAD_SIZE, 0, (struct sockaddr*)&sourceAddr, &srcLen) == -1))
	{
	    perror("recvfrom failed");
	    return 1;
	}
	struct parsed_data parsed = parse(buffer);
	printf("%s\n%s\n%s\n",parsed.username,parsed.time,parsed.message);
	memset(buffer,0,PAYLOAD_SIZE);
    }

    close(sockfd);
    return 0;
}
