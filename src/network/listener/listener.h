#ifndef PACKET_RECEIVER_H
#define PACKET_RECEIVER_H

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include "../GLOBALS.h"
#include "../parser/parser.h"

int start_listener(int port, char* buffer);
struct local_info
{
    int port;
    char* buffer;
};

#endif
