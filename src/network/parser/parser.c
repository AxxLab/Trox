#include "parser.h"

struct parsed_data parse(char* data)
{
    struct parsed_data parsed;
    strcpy(parsed.username, data);
    strcpy(parsed.time, data+strlen(parsed.username)+1);
    strcpy(parsed.message, data+strlen(parsed.username)+strlen(parsed.time)+2);
    return parsed;
}
