#ifndef PARSER_H
#define PARSER_H

#include <stdlib.h>
#include <stdio.h> //printf
#include <string.h>

struct parsed_data
{
    char username[31];
    char time[sizeof(char)*6];
    char message[1024-(sizeof(char)*6+31-1)];
};

struct parsed_data parse(char* data);

#endif
