#include "sender.h"

int send_msg(char* string, char* ip, int port)
{
    struct sockaddr_in destinationAddr;
    int sockfd, dest_size;

    if((sockfd=socket(AF_INET, SOCK_DGRAM, 0)) == -1)
    {
	perror("Something went wrong while creating the socket: ");
	return 1;
    }

    memset((char*)&destinationAddr,0,sizeof(destinationAddr));

    destinationAddr.sin_family=AF_INET;
    destinationAddr.sin_port=htons(1324);
    destinationAddr.sin_addr.s_addr=inet_addr(ip);

    dest_size=sizeof(destinationAddr);

    if(("%d\n",sendto(sockfd,string,PAYLOAD_SIZE,0,(struct sockaddr*)&destinationAddr,sizeof(destinationAddr))) == -1)
    {
	perror("Something went wrong while sending a packet: ");
	return 1;
    }
}
