#ifndef PACKET_SENDER_H
#define PACKET_SENDER_H

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include "../GLOBALS.h"

int send_msg(char* string, char* ip, int port);
struct dest_info
{
    char* string;
    char* ip;
    int port;
} dst_inf;
#endif
